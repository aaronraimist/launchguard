# launchguard

A monitoring bot for space rocket launches, capable to post into matrix and mastodon by token

## working examples
- Mastodon: [https://social.kabi.tk/@launchtospace](https://social.kabi.tk/@launchtospace)
- Matrix: #spaceliveevents:matrix.org