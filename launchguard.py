import launchlibrary as ll
import datetime
import time
import requests
import json

# -- Notification list setup -- #
notification_list = []
# -- Append requests -- #
#
# You can add as many requests as you like
# Each request can define any number of sinks.
# So far, there are the sinks "matrixbytoken" and "mastodonbytoken".
# Further sinks could follow. (Logfile, email, twitter, younameit)
# Each sink can define about which events it will be informed or not.
# Currently there are the following events: launchtoday, launchtomorrow, launchin120minutes,
# launchin60minutes, launchin10minutes, liftoff, canceled, failed und launchwinchanged.
# With the keyword "all", you can match all events belonging to the given "matchtype".
# Explicit naming of the event overrides "all". The following example triggers all events except for "launchtoday":
#  "events" : {"all":1, "launchtoday":0}

# Man kann beliebig viele Requests anfügen (notification_list.append.....)
# Jeder Request, kann beliebig viele Senken (sinks) definieren.
# Bisher gibt es die Senken "matrixbytoken" und "mastodonbytoken".
# Weitere Senken könnten folgen. (Logfile, Email, Twitter, younameit)
# Jede Senke kann definieren, über welche Events sie benachrichtigt wird oder nicht.
# Aktuell gibt es folgende Events: launchtoday, launchtomorrow, launchin120minutes,
# launchin60minutes, launchin10minutes, liftoff, canceled, failed und launchwinchanged.
# Mit dem Keyword "all", kann man alle zum matchtype gehörenden Events matchen.
# Explizite Benennung des Events, überschreibt "all". Folgendes Beispiel triggert alle Events bis auf "launchtoday":
#  "events" : {"all":1, "launchtoday":0}

notification_list.append(
{
  "matchtype" : "launchevent",
  "sinks"     : [
   {
     "sinktype" : "matrixbytoken",
     "server"   : "https://matrix.bau-ha.us",
     "target"   : "!HyWDGzwHQTLAWrYkZY:bau-ha.us",
     "token"    : "GET_YOUR_OWN",
     "events"   : {"all":1}
   },
   {
     "sinktype" : "matrixbytoken",
     "server"   : "https://matrix.bau-ha.us",
     "target"   : "!RZMqsJQZrUvglAeTqg:im.kabi.tk",
     "token"    : "GET_YOUR_OWN",
     "events"   : {"all":1}
   },
   {
     "sinktype" : "mastodonbytoken",
     "server"   : "https://social.kabi.tk",
     "token"    : "GET_YOUR_OWN",
     "events"   : {"all":1}
  }]
})


# -- Functions -- #
def mastodonto(token, api_url, message):
  headers = {}
  headers['Authorization'] = 'Bearer ' + token

  data = {}
  data['status'] = str(message).encode('utf-8')
  data['visibility'] = 'unlisted'

  response = requests.post(url=api_url + '/api/v1/statuses', data=data, headers=headers)
  if response.status_code == 200:
    return True
  else:
    return False

def matrixto(matrix_server, matrix_token, matrix_channel, message):
  if type(message,dict):
    msg       = message["message"]
    nohtmlmsg = message["nohtmlmsg"]
  else:
    msg       = message
    nohtmlmsg = message

  url = matrix_server + "/_matrix/client/r0/rooms/" + matrix_channel + "/send/m.room.message?access_token=" + matrix_token
  headers={}
  req_body = str('{"msgtype":"m.text","body":"' + nohtmlmsg + '","format":"org.matrix.custom.html","formatted_body":"' + msg + '"}').encode('utf-8')
  req = requests.post(url, data=req_body, headers=headers)
  #print(req.content) # For debug

# -- Setup variables -- #
sleepcycle     = 60
progstart      = True
api            = ll.Api()

eventlist      = {
  "launchtoday":{},
  "launchtomorrow":{},
  "launchin120minutes":{},
  "launchin60minutes":{},
  "launchin10minutes":{},
  "liftoff":{},
  "canceled":{},
  "failed":{},
  "launchwinchanged":{}
}

# -- Start main loop -- #
while True:
  launchlist = {}
  launches = ll.Launch.fetch(api, next=5)
  for launch in launches:
    launchlist[launch.id] = launch

  if progstart:
    launchlist_old=launchlist

  # -- Scanning & marking events -- #
  # Phase 1
  # Compare the "now" with the "before" state, to draw conclusions about "events", that have taken place in the meantime.
  # Mark them as "event" in a global table, but don't report them yet.
  for lid,launch in launchlist.items():
    if lid not in launchlist_old:
      print("New launch announced!")
      # Should get own event?
      continue # Break out and start next for run

    if launch.changed != launchlist_old[lid].changed:
      print("Launch data changed!")
      # Should get own event?

    if (launch.status == 3 or launch.status == 6) and (launchlist_old[lid].status  != 3 and launchlist_old[lid].status != 6):
      #print("Liftoff!")
      eventlist["liftoff"][lid] = {"launch":launch, "lastreport":False}

    if (launch.status == 2) and (launchlist_old[lid].status != 2):
      #print("Canceled!")
      eventlist["canceled"][lid] = {"launch":launch, "lastreport":False}

    if (launch.status == 4) and (launchlist_old[lid].status != 4):
      #print("Failed!")
      eventlist["failed"][lid] = {"launch":launch, "lastreport":False}

    if launch.windowstart != launchlist_old[lid].windowstart:
      #print("Launch window changed!")
      eventlist["launchwinchanged"][lid] = {"launch":launch, "lastreport":False}

    if launch.windowstart.date() == datetime.datetime.today().date():
      if not lid in eventlist["launchtoday"]:
        #print("Launch today!")
        eventlist["launchtoday"][lid] = {"launch":launch, "lastreport":False}

    if str(launch.windowstart.date()) == str((datetime.date.today() + datetime.timedelta(days=1))):
      if not lid in eventlist["launchtomorrow"]:
        #print("Launch tomorrow!")
        eventlist["launchtomorrow"][lid] = {"launch":launch, "lastreport":False}

    if launch.net.timestamp() - datetime.datetime.now().timestamp() <= 7200:
      if not lid in eventlist["launchin120minutes"]:
        #print("Launch in 120 minutes!")
        eventlist["launchin120minutes"][lid] = {"launch":launch, "lastreport":False}

    if launch.net.timestamp() - datetime.datetime.now().timestamp() <= 3600:
      if not lid in eventlist["launchin60minutes"]:
        #print("Launch in 60 minutes!")
        eventlist["launchin60minutes"][lid] = {"launch":launch, "lastreport":False}

    if launch.net.timestamp() - datetime.datetime.now().timestamp() <= 600:
      if not lid in eventlist["launchin10minutes"]:
        #print("Launch in 10 minutes!")
        eventlist["launchin10minutes"][lid] = {"launch":launch, "lastreport":False}


  # -- Wipe out events -- #
  # After marking events and to prior to reporting them, we could wipe out some of them, if we wish.
  # For example, if a launch got 2 events, we maybe want to report just one of them and wipe the other out.
  # While no need for this now, we should do this at this point here later, if the demand arises


  # -- Reporting events -- #
  # Phase 2
  # After marking all events and a possible wipe out of some, it is time to finally care about reporting them.
  # Check for all subscriptions (notification_request) for the given event, and report to the demanded sink(s) (matrix, mastodon, etc.)
  for eventname in eventlist:
    for lid, event in eventlist[eventname].items():
      if not event["lastreport"]:
        event["lastreport"] = datetime.datetime.now().timestamp()

        if eventname == "launchtoday":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchtoday"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} ({}) launching today.<br> {}<br> ETA: {}".format(
                        launch.name,
                        launch.agency.name,
                        launch.location.name,
                        str(launch.net)
                      )
                      for mission in launch.missions:
                        message += "<br>  Mission: {}".format(
                          str(mission["description"])
                        )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} ({}) launching today. {} ETA: {}".format(
                        launch.name,
                        launch.agency.name,
                        launch.location.name,
                        str(launch.net)
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "launchtomorrow":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchtomorrow"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} ({}) launching tomorrow.<br> {}<br> ETA: {}".format(
                        launch.name,
                        launch.agency.name,
                        launch.location.name,
                        str(launch.net)
                      )
                      for mission in launch.missions:
                        message += "<br>  Mission: {}".format(
                          str(mission["description"])
                        )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} ({}) launching tomorrow. {} ETA: {}".format(
                        launch.name,
                        launch.agency.name,
                        launch.location.name,
                        str(launch.net)
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "launchin120minutes":
          if not progstart:
            print("T minus 120 minutes!")
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchin120minutes"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} T minus 120 minutes!".format(
                        launch.name
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} T minus 120 minutes!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "launchin60minutes":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchin60minutes"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = launch.name+" T minus 60 minutes!"
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} T minus 60 minutes!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "launchin10minutes":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchin10minutes"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} T minus 10 minutes!".format(
                        launch.name
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} T minus 10 minutes!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "liftoff":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["liftoff"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} liftoff!".format(
                        launch.name
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} liftoff!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "canceled":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["canceled"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} launch canceled!".format(
                        launch.name
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} launch canceled!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "failed":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["failed"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch  = event["launch"]
                      message = "{} launch failed!".format(
                        launch.name
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      message = "{} launch failed!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)

        elif eventname == "launchwinchanged":
          if not progstart:
            for notification_request in notification_list:
              if notification_request["matchtype"] == "launchevent":
                for sink in notification_request["sinks"]:
                  if sink["events"]["all"] or sink["events"]["launchwinchanged"]:
                    if sink["sinktype"] == "matrixbytoken":
                      launch     = event["launch"]
                      launch_old = launchlist_old[lid]
                      message    = "{} launch window changed!<br> Old: {} ETA: {}<br> New: {} ETA: {}".format(
                        launch.name,
                        launch_old.windowstart.strftime("%m/%d, %H:%M:%S")+" - "+launch_old.windowend.strftime("%m/%d, %H:%M:%S"),
                        launch_old.net.strftime("%H:%M:%S"),
                        launch.windowstart.strftime("%m/%d, %H:%M:%S")+" - "+launch.windowend.strftime("%m/%d, %H:%M:%S"),
                        launch.net.strftime("%H:%M:%S")
                      )
                      matrixto(sink["server"], sink["token"], sink["target"], message)
                    elif sink["sinktype"] == "mastodonbytoken":
                      launch  = event["launch"]
                      #updating here too, with new timestamps and a linebreak solution?
                      message = "{} launch window changed!".format(
                        launch.name
                      )
                      mastodonto(sink["token"], sink["server"], message)


# -- Seek & sweep garbage -- #
# From time to time, there will be garbage.
# Old launch ids, that still hanging around in the event table, but don't exist in the actual launchlist anymore (launch done..).
# At this point they should get kicked out.
  for lid,launch in launchlist_old.items():
    if lid not in launchlist:
      print("Launch done!")
      for event in eventlist:
        if lid in eventlist[event]:
          #Garbage found, deleting!
          eventlist[event].pop(lid, None)

  launchlist_old=launchlist
  if progstart:
    progstart = False
  #exit() #Exit hook, if you wish to implement this as cronjob, with serialization to /tmp (future feature).
  time.sleep(sleepcycle)
